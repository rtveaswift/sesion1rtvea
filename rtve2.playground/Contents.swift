//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


/// Función que saluda a persona
///
/// - Parameters:
///   - persona: nombre de la persona
///   - day: día de la semana
/// - Returns: mensaje (e.g. "Hola Pepe, tenga un buen martes")
func greet(person: String, day: String) -> String{
 return ""
}

/// Función que calcula el máximo, el mínimo y la suma total de un array
///
/// - Parameter scores: Array de enteros
/// - Returns: tupla con el máximo, el mínimo y la suma total del parámetro de entrada
func calculateStatistics(scores: [Int]) -> (min: Int, max: Int, sum: Int){
    return (3,3,3)
}


/// Función que devuelva otra función
///
/// - Returns: función que toma de entrada un entero y devuelve un entero
func makeIncrementer() -> ((Int) -> Int)?{
    return nil
}


/// Función que nos indique si todos los elementos de un array cumplen una condición
///
/// - Parameters:
///   - list: Array de enteros a cumplir la condición
///   - condition: función o bloque que devuelva true si la condición para un entero se cumple
/// - Returns: true si todos los elementos cumplen la condición
func hasAnyMatches(list: [Int], condition: (Int) -> Bool) -> Bool{
    return true
}
//Manera swift funcional, alternativa a hacer lo mismo que el anterior
//[1,2,3,-1].reduce(true){ (result: Bool, elem: Int) -> Bool in
//    return result && (elem > 0)
//}

/// Función que ordena un array
///
/// - Parameter array: Enteros a ordenar en orden creciente
/// - Returns: array ordenado en orden creciente (de menor a mayor)
func ordena(array: [Int]) -> [Int]{
    return []
}

/// Función que busca si una cadena se encuentra o no en una lista
///
/// - Parameters:
///   - cadena: cadena a buscar
///   - cadenas: lista de cadenas sobre la que buscar
/// - Returns: True si se encuentra; False en caso contrario
func seEncuentra(cadena: String, enCadenas cadenas: [String]) -> Bool{
    return true
}
//Alternativa funcional...["a","b","c"].filter{ (stri: String) -> Bool in
//    return stri == "a"
//}

/// Declara un protocolo con una variable "informacion" y un metodo "completaInformacion"
protocol ExampleProtocol {
    var informacion: String { get }
    var identificacion: String {get}
    mutating func completaInformacion()
}

/// Declara una clase que implemente el protocolo anterior
class unaClase: ExampleProtocol{
    var informacion: String = "Esto es una clase"
    var identificacion: String = "c1"
    func completaInformacion(){
        
    }
}
struct unaStructura: ExampleProtocol{
    var informacion: String = "Esto es una estructura"
    var identificacion: String = "s1"
    mutating func completaInformacion(){
        informacion += " y necesita mutating"
    }
}
let instancia: unaClase = unaClase()
instancia.completaInformacion()
print(instancia.informacion)

